package es.upm.dit.apsv.traceconsumer2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;

import org.springframework.core.env.Environment;
//import org.springframework.web.client.HttpClientErrorException;
//import org.springframework.web.client.RestTemplate;
import java.util.function.Consumer;
import es.upm.dit.apsv.traceconsumer2.model.Trace;
import es.upm.dit.apsv.traceconsumer2.model.TransportationOrder;
import es.upm.dit.apsv.traceconsumer2.repository.TraceRepository;
import es.upm.dit.apsv.traceconsumer2.repository.TransportationOrderRepository;


@SpringBootApplication
public class Traceconsumer2Application {

        public static final Logger log = LoggerFactory.getLogger(Traceconsumer2Application.class);

        @Autowired
        private  TraceRepository tr;

		@Autowired
        private  TransportationOrderRepository tor;

        @Autowired
        private  Environment env;
	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer2Application.class, args);
	}


        /*@Bean("consumer")
        public Consumer<Trace> checkTrace() {
                return t -> {
                        t.setTraceId(t.getTruck() + t.getLastSeen());
                        tr.save(t);
             };
        }*/

        @Bean("consumer")
        public Consumer<Trace> checkTrace() {
                return t -> {
                        t.setTraceId(t.getTruck() + t.getLastSeen());
                        tr.save(t);
                        TransportationOrder result = null; //Hemos tenido que copiar el modelo de transportation order que habia en order manager al modelo de traceconsumer
                        result = tor.findById(t.getTruck()).orElseThrow();

                        if (result != null && result.getSt() == 0) { //Chequea que el resultado no es nulo y que el estatus es 0 (si es 1 es porque el truck esta en el destino, y queremos que no este en el destino)
                                result.setLastDate(t.getLastSeen());
                                result.setLastLat(t.getLat());
                                result.setLastLong(t.getLng()); //actualiza la posicion del camion
                                if (result.distanceToDestination() < 10) //Si la distancia a destino es menor que 10, le ponemos status a 1 (esta cerca del destino)
                                        {result.setSt(1);}
				tor.save(result);
                        }
                };
        }
}